import React, { useState } from 'react';
import { MDBTable, MDBTableBody, MDBTableHead, MDBProgress } from 'mdbreact';
import { ImagePath } from '../ImagePath';
import { Link, NavLink } from 'react-router-dom';
import 'react-perfect-scrollbar/dist/css/styles.css';
import PerfectScrollbar from 'react-perfect-scrollbar'

function MyDashboard() {

  const [notificationDrawer, setNotificationDrawer] = useState(false); 

  const openNotificationDrawer = () => {
    setNotificationDrawer(true);
  }

  const closeNotificationDrawer = () => {
    setNotificationDrawer(false);
  }

  return (    
        <section className='rightbar-wrap'>
          {notificationDrawer &&
            <div className={notificationDrawer ? "notification-wrap opened" : "notification-wrap closed"} onClick={closeNotificationDrawer}>
                <div className={notificationDrawer ? "notification-sidebar opened" : "notification-sidebar closed"}>
                <div className='searchbar-wrap d-flex justify-content-end'>                   
                    <div className='notification-box d-flex pr-5'>
                      <Link to="#" className='bell'> <span className='dot'></span> <img src={ImagePath.BellIcon} className="img-fluid" alt="icon" /></Link>
                      <span>Aug 20</span>
                    </div>
                  </div>
                  <div className="d-flex justify-content-between align-items-center nhead">
                      <h4>Notifications</h4>
                      <Link to="#">Mark all as read</Link>
                  </div>
                    <PerfectScrollbar className='nscroll'>
                        <ul className="notification-lists list-unstyled">
                            <li>
                                <Link to="#" className="notification-media">
                                    <div className="media-img">
                                      <img src={ImagePath.AvtarImg} className="img-fluid" alt="icon" />
                                    </div>
                                    <div className="media-body position-relative">
                                      <span className="ndate">2 min ago</span>
                                        <h6>Nelly Miller</h6>
                                        <p className="text-truncate">Shared File Name.i2C with someone.</p>
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="#" className="notification-media">
                                    <div className="media-img">
                                      <img src={ImagePath.AvtarImg} className="img-fluid" alt="icon" />
                                    </div>
                                    <div className="media-body position-relative">
                                      <span className="ndate">2 min ago</span>
                                        <h6>Nelly Miller</h6>
                                        <p className="text-truncate">Shared File Name.i2C with someone.</p>
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="#" className="notification-media">
                                    <div className="media-img">
                                      <img src={ImagePath.AvtarImg} className="img-fluid" alt="icon" />
                                    </div>
                                    <div className="media-body position-relative">
                                      <span className="ndate">2 min ago</span>
                                        <h6>Nelly Miller</h6>
                                        <p className="text-truncate">Shared File Name.i2C with someone.</p>
                                    </div>
                                </Link>
                            </li>
                           
                        </ul>
                    </PerfectScrollbar>
                </div>
            </div>
            } 
            {/* end of notification bar */}

          <div className='inner-rightbar position-relative w-100'>
            <div className='searchbar-wrap d-flex justify-content-between'>
              <div className='top-searchbox'>
                <button type='search' className='srch-btn'><img src={ImagePath.SearchIcon} className="img-fluid" alt="icon" /></button>
                <input type="search" className='top-search-input' placeholder='Search...' />
              </div>
              <div className='notification-box d-flex' onClick={openNotificationDrawer}>
                <Link to="#" className='bell'> <span className='dot'></span> <img src={ImagePath.BellIcon} className="img-fluid" alt="icon" /></Link>
                <span>Aug 20</span>
              </div>
            </div>
            <div className='inner-content-part position-relative'>
              <div className='center-part w-100'>
                <h2 className='heading'>Quick Access</h2>

                <ul className='list-unstyled chart-row'>
                  <li>
                    <div className='chart-box'>
                      <div className='bigcircle bluebar'> 20%</div>
                      <span>Restricted</span>
                    </div>
                  </li>
                  <li>
                    <div className='chart-box'>
                      <div className='bigcircle purplebar'> 30%</div>
                      <span>Confidential</span>
                    </div>
                  </li>
                  <li>
                    <div className='chart-box'>
                      <div className='bigcircle peachbar'> 25%</div>
                      <span>Top-Secret</span>
                    </div>
                  </li>
                  <li>
                    <div className='chart-box'>
                      <div className='bigcircle greenbar'> 25%</div>
                      <span>Secret</span>
                    </div>
                  </li>
                </ul>
                <div className='d-flex justify-content-between w-100 align-items-center mb-4'>
                  <h2 className='heading mb-0'>Recent Files</h2>
                  <Link to="#" className='link'>View More</Link>
                </div>
                <div className='table-wrap w-100'>


                  <div className='table-card bg-white'>
                    <MDBTable>
                      <MDBTableHead>
                        <tr>
                          <th>
                            <div className='th-th'>
                              <span>FILE NAME</span>
                              <div className='short-arrow'>
                                <button type='button' className='upshort'></button>
                                <button type='button' className='downshort'></button>
                              </div>
                            </div>
                          </th>
                          <th>
                            <div className='th-th'>
                              <span>CLASSIFICATION</span>
                              <div className='short-arrow'>
                                <button type='button' className='upshort'></button>
                                <button type='button' className='downshort'></button>
                              </div>
                            </div>
                          </th>
                          <th>
                            <div className='th-th'>
                              <span>THROUGH</span>
                              <div className='short-arrow'>
                                <button type='button' className='upshort'></button>
                                <button type='button' className='downshort'></button>
                              </div>
                            </div>
                          </th>
                          <th>
                            <div className='th-th'>
                              <span>CHAIN DATE/TIME</span>
                              <div className='short-arrow'>
                                <button type='button' className='upshort'></button>
                                <button type='button' className='downshort'></button>
                              </div>
                            </div>
                          </th>

                        </tr>
                      </MDBTableHead>
                      <MDBTableBody>
                        <tr>
                          <td>
                            <div className='first-td'>
                              <div className='ftype-box'>.PDF</div>
                              <span>File Name </span>
                              <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                            </div>
                          </td>
                          <td>
                            <div className='second-td'>
                              <h6 className='status-tag'>Confidential</h6>
                            </div>
                          </td>
                          <td>
                            <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                            </div>
                          </td>
                          <td>
                            <div className='fourth-td d-flex '>
                              <span>04-07-2021, 12:10</span>
                              <button type='button' className='more-dot'>...</button>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <div className='first-td'>
                              <div className='ftype-box'>.PDF</div>
                              <span>File Name </span>
                              <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                            </div>
                          </td>
                          <td>
                            <div className='second-td'>
                              <h6 className='status-tag top-secret'>Top-Secret</h6>
                            </div>
                          </td>
                          <td>
                            <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                            </div>
                          </td>
                          <td>
                            <div className='fourth-td d-flex '>
                              <span>04-07-2021, 12:10</span>
                              <button type='button' className='more-dot'>...</button>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <div className='first-td'>
                              <div className='ftype-box'>.PDF</div>
                              <span>File Name </span>
                              <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                            </div>
                          </td>
                          <td>
                            <div className='second-td'>
                              <h6 className='status-tag secret'>Secret</h6>
                            </div>
                          </td>
                          <td>
                            <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                            </div>
                          </td>
                          <td>
                            <div className='fourth-td d-flex '>
                              <span>04-07-2021, 12:10</span>
                              <button type='button' className='more-dot'>...</button>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <div className='first-td'>
                              <div className='ftype-box'>.PDF</div>
                              <span>File Name </span>
                              <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                            </div>
                          </td>
                          <td>
                            <div className='second-td'>
                              <h6 className='status-tag restricted'>Restricted</h6>
                            </div>
                          </td>
                          <td>
                            <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                            </div>
                          </td>
                          <td>
                            <div className='fourth-td d-flex '>
                              <span>04-07-2021, 12:10</span>
                              <button type='button' className='more-dot'>...</button>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <div className='first-td'>
                              <div className='ftype-box'>.PDF</div>
                              <span>File Name </span>
                              <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                            </div>
                          </td>
                          <td>
                            <div className='second-td'>
                              <h6 className='status-tag'>Confidential</h6>
                            </div>
                          </td>
                          <td>
                            <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                            </div>
                          </td>
                          <td>
                            <div className='fourth-td d-flex '>
                              <span>04-07-2021, 12:10</span>
                              <button type='button' className='more-dot'>...</button>
                            </div>
                          </td>
                        </tr>

                      </MDBTableBody>
                    </MDBTable>
                  </div>
                </div>
              </div>
              <div className='filetype-box'>
                <div className='infilebox h-100'>
                  <h3>File type details </h3>

                  <div className='d-flex fchartbox mt-3'>
                    <div className='bigcircle typecircle ml-0'></div>
                    <div className='chartdata pl-3 ml-0'>
                      <small className='d-block'>Available Files </small>
                      <p>700/1000</p>

                      <span className='usedspace'>Used</span>
                      <span className='avilablespace'>Available</span>
                    </div>
                  </div>

                  <ul className='list-unstyled filelists mt-4'>
                    <li>
                      <div className='d-flex align-items-center'>
                        <div className='ftype-box'>.PDF</div>
                        <small>25%</small>
                        <MDBProgress value={25} className="slimbar" />
                      </div>
                    </li>
                    <li>
                      <div className='d-flex align-items-center'>
                        <div className='ftype-box'>.DOC</div>
                        <small>20%</small>
                        <MDBProgress value={20} className="slimbar" />
                      </div>
                    </li>
                    <li>
                      <div className='d-flex align-items-center'>
                        <div className='ftype-box'>.PNG</div>
                        <small>65%</small>
                        <MDBProgress value={65} className="slimbar" />
                      </div>
                    </li>
                    <li>
                      <div className='d-flex align-items-center'>
                        <div className='ftype-box'>.XLSX</div>
                        <small>15%</small>
                        <MDBProgress value={15} className="slimbar" />
                      </div>
                    </li>
                    <li>
                      <div className='d-flex align-items-center'>
                        <div className='ftype-box'>.PPT</div>
                        <small>55%</small>
                        <MDBProgress value={55} className="slimbar" />
                      </div>
                    </li>

                  </ul>

                  <div className='abs-btn-box text-center'>
                    <span>Want to chain more files?</span>
                    <button type='button' className='theme-btn btn btn-block'>Upgrade Now</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>    
  );
}

export default MyDashboard;
