import React from 'react';
import { MDBTable, MDBTableBody, MDBTableHead,  MDBProgress } from 'mdbreact';
import { ImagePath } from '../ImagePath';
import { Link, NavLink } from 'react-router-dom';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, UncontrolledDropdown, ModalBody } from 'reactstrap';

function MailStared() {
  return (
        <section className='rightbar-wrap'>
            <div className='center-part w-100'>
              <div className='d-flex justify-content-between w-100 align-items-center mt-5'>
                <h2 className='heading mb-0'>View your MailStared</h2>
              </div>
                
            </div>
          </section>
  );
}

export default MailStared;
