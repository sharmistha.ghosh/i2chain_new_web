import React from 'react';
import { MDBTable, MDBTableBody, MDBTableHead,  MDBProgress } from 'mdbreact';
import { ImagePath } from '../ImagePath';
import { Link, NavLink } from 'react-router-dom';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, UncontrolledDropdown } from 'reactstrap';
import Divider from "@material-ui/core/Divider";

function ChainedDocument() {
  return (
        <section className='rightbar-wrap'>
          <div className='searchbar-wrap d-flex justify-content-between'>
            <div className='top-searchbox'>
              <button type='search' className='srch-btn'><img src={ImagePath.SearchIcon} className="img-fluid" alt="icon" /></button>
              <input type="search" className='top-search-input' placeholder='Search...' />
            </div>
            <div className='notification-box d-flex'>
              <Link to="#" className='bell'> <span className='dot'></span> <img src={ImagePath.BellIcon} className="img-fluid" alt="icon" /></Link>
              <span>Aug 20</span>
            </div>
            </div>
            <div className='w-100'>
              <div className='d-flex justify-content-between w-100 align-items-center mb-4'>
                <h2 className='heading mb-0'>Chained Documents</h2>
                <UncontrolledDropdown setActiveFromChild className="custom">
                  <DropdownToggle tag="a" className="nav-link" caret>
                    <Link to="#" className='link'> Sort</Link>
                  </DropdownToggle>
                  <DropdownMenu right className="custom">
                    <DropdownItem tag="a" href="/blah" >Name</DropdownItem>
                    <DropdownItem tag="a" href="/blah" >Date</DropdownItem>
                    <DropdownItem tag="a" href="/blah" >File Type</DropdownItem>
                    <DropdownItem tag="a" href="/blah" >Classification</DropdownItem>
                    <Divider className='my-2' />
                    <DropdownItem tag="a" href="/blah" >Ascending</DropdownItem>
                    <DropdownItem tag="a" href="/blah" >Descending</DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </div>
              <div className='table-wrap w-100'>
          
              
                <div className='table-card bg-white'>
                  <MDBTable>  
                  <MDBTableHead>
                        <tr>
                          <th>
                            <div className='th-th'>
                              <span>FILE NAME</span>
                              <div className='short-arrow'>
                                <button type='button' className='upshort'></button>
                                <button type='button' className='downshort'></button>
                              </div>
                            </div>
                          </th>
                          <th>
                            <div className='th-th'>
                              <span>CLASSIFICATION</span>
                              <div className='short-arrow'>
                                <button type='button' className='upshort'></button>
                                <button type='button' className='downshort'></button>
                              </div>
                            </div>
                          </th>
                          <th>
                            <div className='th-th'>
                              <span>THROUGH</span>
                              <div className='short-arrow'>
                                <button type='button' className='upshort'></button>
                                <button type='button' className='downshort'></button>
                              </div>
                            </div>
                          </th>
                          <th>
                            <div className='th-th'>
                              <span>CHAIN DATE/TIME</span>
                              <div className='short-arrow'>
                                <button type='button' className='upshort'></button>
                                <button type='button' className='downshort'></button>
                              </div>
                            </div>
                          </th>

                        </tr>
                      </MDBTableHead>                    
                    <MDBTableBody>
                      <tr>
                        <td>
                          <div className='first-td'>
                            <div className='ftype-box'>.PDF</div>
                            <span>File Name </span>
                            <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                          </div>
                          </td>
                        <td>
                          <div className='second-td'>
                              <h6 className='status-tag'>Confidential</h6>
                          </div>
                        </td>
                        <td>
                          <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                          </div>
                        </td>
                        <td>
                          <div className='fourth-td d-flex '>
                            <span>04-07-2021, 12:10</span>
                            <button type='button' className='more-dot'>...</button>
                          </div>
                        </td>                         
                      </tr>   
                      <tr>
                        <td>
                          <div className='first-td'>
                            <div className='ftype-box'>.PDF</div>
                            <span>File Name </span>
                            <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                          </div>
                          </td>
                        <td>
                          <div className='second-td'>
                              <h6 className='status-tag top-secret'>Top-Secret</h6>
                          </div>
                        </td>
                        <td>
                          <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                          </div>
                        </td>
                        <td>
                          <div className='fourth-td d-flex '>
                            <span>04-07-2021, 12:10</span>
                            <button type='button' className='more-dot'>...</button>
                          </div>
                        </td>                         
                      </tr>   
                      <tr>
                        <td>
                          <div className='first-td'>
                            <div className='ftype-box'>.PDF</div>
                            <span>File Name </span>
                            <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                          </div>
                          </td>
                        <td>
                          <div className='second-td'>
                              <h6 className='status-tag secret'>Secret</h6>
                          </div>
                        </td>
                        <td>
                          <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                          </div>
                        </td>
                        <td>
                          <div className='fourth-td d-flex '>
                            <span>04-07-2021, 12:10</span>
                            <button type='button' className='more-dot'>...</button>
                          </div>
                        </td>                         
                      </tr>   
                      <tr>
                        <td>
                          <div className='first-td'>
                            <div className='ftype-box'>.PDF</div>
                            <span>File Name </span>
                            <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                          </div>
                          </td>
                        <td>
                          <div className='second-td'>
                              <h6 className='status-tag restricted'>Restricted</h6>
                          </div>
                        </td>
                        <td>
                          <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                          </div>
                        </td>
                        <td>
                          <div className='fourth-td d-flex '>
                            <span>04-07-2021, 12:10</span>
                            <button type='button' className='more-dot'>...</button>
                          </div>
                        </td>                         
                      </tr>   
                      <tr>
                        <td>
                          <div className='first-td'>
                            <div className='ftype-box'>.PDF</div>
                            <span>File Name </span>
                            <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                          </div>
                          </td>
                        <td>
                          <div className='second-td'>
                              <h6 className='status-tag'>Confidential</h6>
                          </div>
                        </td>
                        <td>
                          <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                          </div>
                        </td>
                        <td>
                          <div className='fourth-td d-flex '>
                            <span>04-07-2021, 12:10</span>
                            <button type='button' className='more-dot'>...</button>
                          </div>
                        </td>                         
                      </tr>   
                      <tr>
                        <td>
                          <div className='first-td'>
                            <div className='ftype-box'>.PDF</div>
                            <span>File Name </span>
                            <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                          </div>
                          </td>
                        <td>
                          <div className='second-td'>
                              <h6 className='status-tag'>Confidential</h6>
                          </div>
                        </td>
                        <td>
                          <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                          </div>
                        </td>
                        <td>
                          <div className='fourth-td d-flex '>
                            <span>04-07-2021, 12:10</span>
                            <button type='button' className='more-dot'>...</button>
                          </div>
                        </td>                         
                      </tr>   
                      <tr>
                        <td>
                          <div className='first-td'>
                            <div className='ftype-box'>.PDF</div>
                            <span>File Name </span>
                            <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                          </div>
                          </td>
                        <td>
                          <div className='second-td'>
                              <h6 className='status-tag top-secret'>Top-Secret</h6>
                          </div>
                        </td>
                        <td>
                          <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                          </div>
                        </td>
                        <td>
                          <div className='fourth-td d-flex '>
                            <span>04-07-2021, 12:10</span>
                            <button type='button' className='more-dot'>...</button>
                          </div>
                        </td>                         
                      </tr>   
                      <tr>
                        <td>
                          <div className='first-td'>
                            <div className='ftype-box'>.PDF</div>
                            <span>File Name </span>
                            <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                          </div>
                          </td>
                        <td>
                          <div className='second-td'>
                              <h6 className='status-tag secret'>Secret</h6>
                          </div>
                        </td>
                        <td>
                          <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                          </div>
                        </td>
                        <td>
                          <div className='fourth-td d-flex '>
                            <span>04-07-2021, 12:10</span>
                            <button type='button' className='more-dot'>...</button>
                          </div>
                        </td>                         
                      </tr>   
                      <tr>
                        <td>
                          <div className='first-td'>
                            <div className='ftype-box'>.PDF</div>
                            <span>File Name </span>
                            <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                          </div>
                          </td>
                        <td>
                          <div className='second-td'>
                              <h6 className='status-tag restricted'>Restricted</h6>
                          </div>
                        </td>
                        <td>
                          <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                          </div>
                        </td>
                        <td>
                          <div className='fourth-td d-flex '>
                            <span>04-07-2021, 12:10</span>
                            <button type='button' className='more-dot'>...</button>
                          </div>
                        </td>                         
                      </tr>   
                      <tr>
                        <td>
                          <div className='first-td'>
                            <div className='ftype-box'>.PDF</div>
                            <span>File Name </span>
                            <span className='tipicon'><img src={ImagePath.TipIcon} className="img-fluid" alt="icon" /></span>
                          </div>
                          </td>
                        <td>
                          <div className='second-td'>
                              <h6 className='status-tag'>Confidential</h6>
                          </div>
                        </td>
                        <td>
                          <div className='third-td'>
                              <h6 className='status-grey'>SharePoint</h6>
                          </div>
                        </td>
                        <td>
                          <div className='fourth-td d-flex '>
                            <span>04-07-2021, 12:10</span>
                            <button type='button' className='more-dot'>...</button>
                          </div>
                        </td>                         
                      </tr>   

                    </MDBTableBody>
                  </MDBTable>
                </div>                  
              </div>
            </div>
          </section>
        
  );
}

export default ChainedDocument;
