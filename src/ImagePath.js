import Logo from '../src/Assets/images/logo.svg';
import ProfileImage from '../src/Assets/images/profile-pic.png';
import NavIcon1 from '../src/Assets/images/icons/home-icon.svg';
import NavIcon2 from '../src/Assets/images/icons/document-icon.svg';
import NavIcon3 from '../src/Assets/images/icons/mail.svg';
import NavIcon4 from '../src/Assets/images/icons/i2c.svg';
import NavIcon5 from '../src/Assets/images/icons/help.svg';
import NavIcon6 from '../src/Assets/images/icons/setting.svg';
import NavIcon7 from '../src/Assets/images/icons/logout.svg';
import SearchIcon from '../src/Assets/images/icons/search.svg';
import BellIcon from '../src/Assets/images/icons/bell.svg';
import TipIcon from '../src/Assets/images/icons/tip.svg';
import MailLogo1 from '../src/Assets/images/icons/green-mail-logo.svg';
import MailLogo2 from '../src/Assets/images/icons/mail-logo2.svg';
import Star from '../src/Assets/images/icons/star.svg';
import DemoImg from '../src/Assets/images/icons/attachment-icon.svg';
import AvtarImg from '../src/Assets/images/avtar.svg'




export const ImagePath = { 
    Logo:Logo,
    ProfileImage:ProfileImage,
    NavIcon1:NavIcon1,
    NavIcon2:NavIcon2,
    NavIcon3:NavIcon3,
    NavIcon4:NavIcon4,
    NavIcon5:NavIcon5,
    NavIcon6:NavIcon6,
    NavIcon7:NavIcon7,
    SearchIcon:SearchIcon,
    BellIcon:BellIcon,
    TipIcon:TipIcon,
    MailLogo1:MailLogo1,
    MailLogo2:MailLogo2,
    Star:Star,
    DemoImg:DemoImg,
    AvtarImg:AvtarImg,
    

}